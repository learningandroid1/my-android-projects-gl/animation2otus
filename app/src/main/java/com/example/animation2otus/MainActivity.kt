package com.example.animation2otus

import android.animation.Animator
import android.animation.AnimatorInflater
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.graphics.Interpolator
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.animation.*
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    lateinit var square: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        square = findViewById(R.id.square)
        square.setOnClickListener{
            Toast.makeText(this, "Нажаль)", Toast.LENGTH_LONG).show()
        }

        //doValueAnimator()
        //doValueAnimator2()
        //doObjectAnimator()
        //doAnimatorFromXml()
        //doPropertyAnimation()
        doXmlAnimation()
    }

    private fun doValueAnimator() {
        ValueAnimator.ofFloat(0f, 200f)
            .apply {
                duration = 1000
                repeatCount = ValueAnimator.INFINITE
                repeatMode = ValueAnimator.REVERSE
                interpolator = LinearInterpolator()


                addUpdateListener {
                    val value = it.animatedValue as Float
                    square.translationX = value
                    square.translationY = value
                }
            }
            .start()
    }

    private fun doValueAnimator2() {
        ValueAnimator.ofFloat(0f, 2f)
            .apply {
                duration = 3000
                repeatCount = 1
                repeatMode = ValueAnimator.REVERSE
                interpolator = BounceInterpolator()


                addUpdateListener {
                    val value = it.animatedValue as Float
                    square.scaleX = value
                    square.scaleY = value
                }
            }
            .start()
    }

    private fun doObjectAnimator(){
        ObjectAnimator.ofFloat(square, "translationX", 200f)
            .apply{
                duration = 3000
                repeatCount = 2
                repeatMode = ValueAnimator.REVERSE
                interpolator = AccelerateDecelerateInterpolator()

                addListener(object: Animator.AnimatorListener{
                    override fun onAnimationStart(animation: Animator?){
                        Log.d("Test---->", "onAnimationStart")
                    }

                    override fun onAnimationEnd(p0: Animator?) {
                        Log.d("Test---->", "onAnimationEnd")
                    }

                    override fun onAnimationCancel(p0: Animator?) {
                        Log.d("Test---->", "onAnimationCancel")
                    }

                    override fun onAnimationRepeat(p0: Animator?) {
                        Log.d("Test---->", "onAnimationRepeat")
                    }
                })
            }
            .start()
    }

    private fun doAnimatorFromXml(){
        val animator = AnimatorInflater.loadAnimator(this, R.animator.simple_animator)
        animator.setTarget(square)
        animator.start()
    }

    private fun doPropertyAnimation(){
        square.animation = ScaleAnimation(0f, 1f, 0f, 1f)
            .apply {
                duration = 1000
                repeatCount = 2
                repeatMode = ValueAnimator.REVERSE
                start()
            }
    }

    private fun doXmlAnimation(){
       val animation = AnimationUtils.loadAnimation(this, R.anim.fade_out_scale)
        square.animation = animation
        square.animation.start()
    }
}